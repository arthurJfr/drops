<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'dropship' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wO0)hfFDE&_>[p5eaH/yq&!_r#da7~~&hK%,^FJLl[hR[RqyM~bG#G~(XFgmKyF9' );
define( 'SECURE_AUTH_KEY',  'Z.J>C$9&QlHsj@uErXdhr&P.TIML@y?VKQW+H&3&z(@ _e?1R*T_c^,svzgi;H9~' );
define( 'LOGGED_IN_KEY',    'Wk)_<BGz5+XzW8oo[hktmf!?SQOrY#$}YQK{,:gJe7<w40Ajqf;Y {QKXs~h%@dt' );
define( 'NONCE_KEY',        '5,.Xi<$C*#:[YUE-TU;>!I]g^:B;8ROfveq}b*!d8IMZSK57w{d}^9Ei[l;daKbh' );
define( 'AUTH_SALT',        '1=IIe7u^s^<mmahuowwX>FMeijdwj_1YfJ 62&b8)Ye[DwGvH7|w LN&]r~61:U:' );
define( 'SECURE_AUTH_SALT', 'uwVC s~W;r6gy) r&>A~z~;U0;V^QZm2(>J@:wqR>m)ibPPZViEvkrcoaRr*2?=<' );
define( 'LOGGED_IN_SALT',   '8?{d(k=`+=$Z7(bzav-ko]P%[H9d6%i)or.gq/YkEvj+w7~]l>03|4rg`X;[V[EW' );
define( 'NONCE_SALT',       'ekF9.s;g6>U Ydpj0fCkmHI]>3P`hmq:4#!0cYfXS.QUxhTs:#?C62gx-7LA7e{~' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'ds_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
